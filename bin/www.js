// server.js
const jsonServer = require('json-server');
const fs = require('q-io/fs');
const parseString = require('xml2js').parseString;

async function readXmlDir (dirName) {
	const xmlContents = [];

	const fileList = await fs.list(dirName);
	for (let fileName of fileList) {
		const stat = await fs.stat(dirName + fileName);
		if (stat.isFile()) {
			const fileContent = await fs.read(dirName + fileName);
			const xml = await (new Promise((resolve, reject) => {
				parseString(fileContent, (err, result) => err ? reject(err) : resolve(result));
			}));
			
			xmlContents.push({
				fileName,
				xml
			})
		}
	}

	return xmlContents;
}

async function init () {
	const modulesFolder = './modules/';

	const modules = (await readXmlDir('./modules/'))
		.filter(file => file.fileName !== 'main.xml')
		.map(file => ({
			module: file.fileName.replace('.xml', ''),
			title: file.fileName.replace('.xml', '')
		}));

	const endpoints = (await readXmlDir('./endpoints/'))
		.reduce((acc, curr) => acc.concat(curr.xml.endpoints.item), [])
		.map(item => item.$);

	const resources = (await readXmlDir('./model/'))
		.map(item => item.fileName.replace('.xml', ''));

	const resourceRoutes = resources.reduce((acc, curr) => Object.assign(acc, { [curr]: [] }), {});

	for (let i in resourceRoutes) {
		const fileName = './data/resource/' + i + '.json';
		const isFile = await fs.isFile(fileName);
		if (isFile) {
			const data = await fs.read(fileName);
			resourceRoutes[i] = JSON.parse(data);
		}
	}

	const resourceRewrites = resources.reduce((acc, curr) => Object.assign(acc, {
		['/resource/' + curr + '/:id']: '/' + curr + '/:id',
		['/resource/' + curr]: '/' + curr
	}), {});

	const endpointInfo = resources.map(res => ({
		name: 'resource' + res.charAt(0).toUpperCase() + res.slice(1),
		path: 'resource/' + res + '/:id',
		rule: []
	}))
	
	const customRoutes = endpoints.reduce((acc, curr) => Object.assign(acc, { [curr.name]: curr.path }), {});

	for (let i in customRoutes) {
		const fileName = './data/' + customRoutes[i].replace('/:id', '') + '.json';
		const isFile = await fs.isFile(fileName);
		if (isFile) {
			const data = await fs.read(fileName);
			customRoutes[i] = JSON.parse(data);
		} else {
			customRoutes[i] = [];
		}
	}

	const customRewrites = endpoints.reduce((acc, curr) => Object.assign(acc, {
		['/' + curr.path.replace(':id', '*')]: '/' + curr.name + '/$1',
		['/' + curr.path.replace('/:id', '')]: '/' + curr.name
	}), {});

	const server = jsonServer.create();
	const router = jsonServer.router(Object.assign({}, resourceRoutes, customRoutes, {
		login: {
			menu: modules,
			user: {
				id: 1,
				email: 'test@test.cz'
			}
		},
		'endpoint-info': endpointInfo
	}));

	const middlewares = jsonServer.defaults();

	server.use(middlewares);

	server.use(jsonServer.rewriter(Object.assign({
		'/api/*': '/$1'
	}, resourceRewrites, customRewrites)));

	server.use(router);
	server.listen(3700, () => {
		console.log('JSON Server is running');
	});
}

init();