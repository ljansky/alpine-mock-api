FROM node:alpine

RUN apk add --update bash && rm -f /var/cache/apk/*

RUN npm install pm2 -g

COPY package.json /usr/src/app/package.json
RUN cd /usr/src/app && npm install

COPY process.json /usr/src/app/process.json
COPY bin/ /usr/src/app/bin/

WORKDIR /usr/src/app

EXPOSE 3700

ENTRYPOINT ["npm",  "start"]